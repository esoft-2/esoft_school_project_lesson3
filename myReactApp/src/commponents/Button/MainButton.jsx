import React, { useState } from "react"; // Всё очень плохо с разграничением компонентов и я запутался в том как их правильно скомпоновать. Основную задачу делает, но я думаю очень-очень не эффективно. Пока не нашёл другого решения
import "../../App.css"
import { Skils } from "../List/Skils";

const skils = [
    {id: 0, title: "Js", discription: "Базовае знание языка и его примениения", level: 100},
    {id: 1, title: "Ts", discription: "Базовае знание языка и его примениения", level: 0},
    {id: 2, title: "React", discription: "Умение применять и понипать что происходит", level: 40},
    {id: 3, title: "Node js", discription: "Всё так же умение приминять и понимать", level: 50},
]

export const MainButton = () => {

    
    const [state, setState] = useState(true);

    const onClick = () => setState((prev) => !prev)



    let [stateButton, setStateButton] = useState(skils)
    
    const over50 = () => {
        const newSkilsArr = stateButton.filter(item => item.level >= 50)
        setStateButton(newSkilsArr)
        stateButton = skils
    }
    const menee50 = () => {
        const newSkilsArr = stateButton.filter(item => item.level < 50)
        setStateButton(newSkilsArr)
        stateButton = skils
    }

    const reset = () => {
        stateButton = skils
        setStateButton(stateButton)
    }




    if (state) {
        return(
            <button onClick={onClick}>
                Показать компетенции
            </button>
        )
    }
    return(
        <>
            <button onClick={onClick}>
                Убрать компетенции
            </button>
            <div>
                <button onClick={over50}>Уровень компетенции 50 и более</button>
                <button onClick={reset}>Сброс фильтра</button> 
                <button onClick={menee50}>Уровень компетенции менее 50</button>
            </div>
            <div className="main-item-div">
                <Skils arr={stateButton}/>
            </div>
        </>
    )  
}

