import React from "react";

export const Item = ({title, discription, level}) => {
    return(
        <div className="item-div">
            <h1> {title} </h1>
            <p> {discription} </p>
            <h2> {level}% </h2>
        </div>
    )
}