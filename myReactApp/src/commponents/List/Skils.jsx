import React, {useState} from "react";
import { Item } from "./Item";



export const Skils = ({arr}) => {



    return arr.map(item => (
        <Item
            key={item.id}
            title = {item.title}
            discription = {item.discription}
            level = {item.level}
        />
    ))
}